#REGRESION LOGISTICA

mydata0<-read.table("./german.data-numeric.csv",sep=",")
set.seed(2131)
mydata <- mydata0[sample(nrow(mydata0), 700), ] 
mytest <- mydata0[ !(rownames(mydata0) %in% rownames(mydata)), ] 

x1<-mydata$V2
x2<-mydata$V4
y<-2-mydata$V25

train<-data.frame(y,x1,x2)
logistic<-glm(y~x1+x2,data=train,family=binomial) #GLM es el modelo que saca todos los coeficientes

x1<-mytest$V2
x2<-mytest$V4
y<-2-mytest$V25

test<-data.frame(y,x1,x2)

mytest["logistic"]<-predict(logistic,newdata=test,type="response")

beta0<- 0.88724
beta1<- -0.00765
beta2<- -0.0008

for (i in 1:nrow(mytest)){
  row<-mytest[i,]
  x1<-row[[i,"V2"]]
  x2<-row[[i,"V4"]]
  y_modelo<-1/(1+exp(-(beta0+beta1*x1+beta2*x2)))
  y_modelo<-mytest[rownames(row),"manual_log"]
}

good_test <- mytest[mytest["V25"]==1,]
bad_test <- mytest[(mytest["V25"])==2,]

breaks<-c(0.28,0.39,0.50,0.61,0.72,0.83)
gh <- hist(good_test$logistic, breaks=breaks)
bh <- hist(bad_test$logistic, breaks = breaks)

x <- gh$mids 
y <- cumsum(gh$counts)/(cumsum(gh$counts) + cumsum(bh$counts))

plot(x=x,y=y,main = "Pureza")
lines(x,y)

x <- gh$mids 
y <- cumsum(gh$counts+bh$counts)/sum(gh$counts + bh$counts)

plot(x=x,y=y,main = "Eficiencia")
lines(x,y)
