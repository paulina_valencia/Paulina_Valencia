#regresion con 3 variables c/eficiencia y pureza

library(MASS)
mydata0 <- read.table("german.data-numeric.csv",sep=",")
set.seed(1232)
mydata <- mydata0[sample(nrow(mydata0), 700),] # elige aleatoriamente 700 datos para entrenar el modelo
mytest <- mydata0[!(rownames(mydata0) %in% rownames(mydata)),] # elije el resto de los datos para probar el modelo
xdata <- mydata$V2
x2data <- mydata$V4
x3data <- mydata$V10
ydata <- 2-mydata$V25

xtx11 <- sum( xdata*xdata)
xtx12 <- sum (xdata*x2data)
xtx21 <- xtx12
xtx22 <- sum(x2data*x2data)

xtx00<- length(xdata)
xtx01 <- sum(xdata)
xtx10 <-xtx01
xtx02 <- sum(x2data)
xtx20 <-xtx02

xtx03<-sum(x3data)
xtx30<-xtx03
xtx33<-sum(x3data*x3data)
xtx13<-sum(xdata*x3data)
xtx23<- sum(x2data*x3data)
xtx31<-xtx13
xtx32<-xtx23

xy0 <- sum(ydata)
xy1 <- sum(xdata*ydata)
xy2 <- sum(x2data*ydata)
xy3 <- sum(x3data*ydata)

XTX <- rbind(c(xtx00,xtx01,xtx02,xtx03),c(xtx10,xtx11,xtx12,xtx13),c(xtx20,xtx21,xtx22,xtx23),c(xtx30,xtx31,xtx31,xtx33)) #crea una matriz de 9x9

invxtx <- ginv(XTX) #inversa de la nmatriz

YX <- rbind(c(xy0),c(xy1),c(xy2),c(xy3))
print(YX)

betas <- invxtx %*% YX

testx1<- mytest$v2
testx2<- mytest$v4
testx3<- mytest$v10

for (i in 1:nrow(mytest)) {
  row<- mytest[i,]
  x1 <- mytest[i,"V2"]
  x2 <- mytest[i,"V4"]
  x3 <- mytest[i,"V10"]
  ymodel <- betas[1]+betas[2]*x1+betas[3]*x2+betas[4]*x3
  mytest[rownames(row),"linnear_regression"] <- betas[1] + betas[2]*x1 + betas[3]*x2+ betas[4]*x3
} #este for rellena un vector con los resultados de probar el modelo con los datos de prueba

breaks <- c(0,0.20,0.40,0.60,0.80,1)  #rango de tu variable/ numero de bits 
good_test <- mytest[mytest["V25"]==1,]
bad_test <- mytest[(2-mytest["V25"])==0,]

gh <- hist(good_test$linnear_regression, breaks=breaks)
bh <- hist(bad_test$linnear_regression, breaks = breaks)

x <- gh$mids 
y <- cumsum(gh$counts)/(cumsum(gh$counts) + cumsum(bh$counts))

plot(x=x,y=y,main = "Pureza")
lines(x,y)

x <- gh$mids 
y <- cumsum(gh$counts)/sum(gh$counts + bh$counts)

plot(x=x,y=y,main = "Eficiencia")
lines(x,y)

